# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack to unpin OneDrive from Windows Explorer.  The idea is to provide a tool to change this setting after a fresh install of Windows.
# Original source: https://www.howtogeek.com/225973/how-to-disable-onedrive-and-remove-it-from-file-explorer-on-windows-10/
# Use Case: To be run once on each machine.  A reboot is required for the change to take effect.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Adding the registry key, setting it to a value of 0.  If the registry key does not exist already in the system, then it will be created.
[Microsoft.Win32.Registry]::SetValue('HKEY_CLASSES_ROOT\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}', 'System.IsPinnedToNameSpaceTree', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
# Repeating the same for 64-bit machines.
[Microsoft.Win32.Registry]::SetValue('HKEY_CLASSES_ROOT\Wow6432Node\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}', 'System.IsPinnedToNameSpaceTree', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
