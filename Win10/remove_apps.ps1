# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a batch uninstaller of Windows Store apps that are not desired on a fresh install of Windows.
# Use Case: To be run once on each user profile by an account with local admin privileges.
# NOTE: To view all of the apps on the machine that can be removed:
# Get-AppxPackage -AllUsers | Where-Object NonRemovable -eq $false | Sort-Object Name | Select Name, PackageFamilyName
# NOTE: To view all of the apps on the machine:
# Get-AppxPackage -AllUsers | Sort-Object NonRemovable, Name | Select NonRemovable, Name, PackageFamilyName


# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# List of apps that are to be removed.
$AppList =	"king.com.CandyCrushFriends",
			"king.com.FarmHeroesSaga",
			# "Microsoft.AAD.BrokerPlugin",	# Installed already; removal is not supported.  Manual removal required by "Turn Windows Features On or Off".
			"Microsoft.Advertising.Xaml",	# Adding as of W10 20H2.
			"Microsoft.BingFinance",	# Not installed already; but nice to not have.
			"Microsoft.BingNews",	# Not installed already; but nice to not have.
			"Microsoft.BingSports",	# Not installed already; but nice to not have.
			"Microsoft.BingWeather",	# Installed already; needs to be removed.
			"Microsoft.GetHelp",	# Installed already; unnecessary.
			"Microsoft.GetStarted",	# Installed already; unnecessary.
			"Microsoft.Messaging",	# Installed already; needs to be removed.
			"Microsoft.Microsoft3DViewer",	# Installed already; unnecessary.
			"Microsoft.MicrosoftOfficeHub",	# Installed already; needs to be removed.
			"Microsoft.MicrosoftSolitaireCollection",	# Installed already; needs to be removed.
			"Microsoft.MicrosoftStickyNotes",	# Installed already; needs to be removed.
			"Microsoft.MixedReality.Portal",	# Installed already; unnecessary.
			"Microsoft.MSPaint",	# Installed already; unnecessary.
			"Microsoft.Office.OneNote",	# Installed already; needs to be removed.
			"Microsoft.OneConnect",	# Installed already; needs to be removed.
			"Microsoft.People",	# Installed already; needs to be removed.
			"Microsoft.Print3D",	# Installed already; unnecessary.
			"Microsoft.ScreenSketch",	# Installed already; unnecessary, made redundant by the Snipping Tool.
			"Microsoft.SkypeApp",	# Installed already; needs to be removed.
			"Microsoft.UI.Xaml.2.0",	# Adding as of W10 20H2.
			"Microsoft.Wallet",	# Installed already; not sure if it should be removed or not.
			# "Microsoft.Windows.ParentalControls",	# Installed already; removal is not supported.  Manual removal required by "Turn Windows Features On or Off".
			"Microsoft.Windows.Photos",	# Installed already; unnecessary, made redundant by 3rd party image viewer.
			"Microsoft.WindowsAlarms",	# Installed already; not sure if it should be removed or not.
			"Microsoft.WindowsCalculator",	# Installed already; unnecessary, made redundant by 3rd party calculator.
			"Microsoft.WindowsCamera",	# Installed already, not sure if it should be removed or not.
			"microsoft.windowscommunicationsapps",	# Installed already; unnecessary, made redundant by web-based email apps.
			"Microsoft.WindowsFeedbackHub",	# Installed already, not sure if it should be removed or not.
			"Microsoft.WindowsMaps",	# Installed already; not sure if it should be removed or not.
			"Microsoft.WindowsSoundRecorder",	# Installed already; not sure if it should be removed or not.
			"Microsoft.XboxApp",	# Installed already; needs to be removed.
			"Microsoft.XboxGameOverlay",	# Installed already; needs to be removed.
			"Microsoft.XboxGamingOverlay",	# Installed already; needs to be removed.
			"Microsoft.XboxIdentityProvider",	# Installed already; needs to be removed.
			"Microsoft.XboxSpeechToTextOverlay",	# Adding as of W10 20H2.
			"Microsoft.YourPhone",	# Installed already; needs to be removed.
			"Microsoft.ZuneMusic",	# Installed already; needs to be removed.
			"Microsoft.ZuneVideo",	# Installed already; needs to be removed.
			"Microsoft.549981C3F5F10",	# Adding as of W10 20H2.  This is the "app" for Cortana.
			# "Windows.CBSPreview",	# Installed already; removal is not supported.  Manual removal required by "Turn Windows Features On or Off".
			"SpotifyAB.SpotifyMusic",	# Found it installed after running config scripts etc after a new install.  Not sure if it was added during install or after, or of the source of its addition.  Remove with prejudice.
			"Windows.ContactSupport"	# Not installed already; but nice to not have.

# Other apps to look into:

# MS Hello Setup UI:
# Microsoft.BioEnrollment
# Microsoft.CredDialogHost
# Microsoft.ECApp
# Microsoft.LockApp

# Cortana:
# Microsoft.Windows.Cortana
# Microsoft.Windows.Holographic.FirstRun
# Microsoft.Windows.OOBENetworkCaptivePort
# Microsoft.Windows.OOBENetworkConnectionFlow
# Microsoft.Windows.ParentalControls

# Microsoft Advertising:
# Microsoft.Advertising.Xaml
# Microsoft.NET.Native.Framework.1.2
# Microsoft.NET.Native.Framework.1.3
# Microsoft.NET.Native.Framework.1.6
# Microsoft.NET.Native.Framework.1.7
# Microsoft.NET.Native.Framework.2.0
# Microsoft.NET.Native.Runtime.1.1
# Microsoft.NET.Native.Runtime.1.3
# Microsoft.NET.Native.Runtime.1.4
# Microsoft.NET.Native.Runtime.1.6
# Microsoft.NET.Native.Runtime.1.7
# Microsoft.NET.Native.Runtime.2.0
# Microsoft.Services.Store.Engagement
# Microsoft.VCLibs.120.00
# Microsoft.VCLibs.140.00
# Microsoft.VCLibs.120.00.Universal
# Microsoft.VCLibs.140.00.UWPDesktop

# Check to see if each app is installed.  If installed, remove it.  Otherwise, note it and move on.
ForEach ($App in $AppList) {
	Write-Host "App: $App"
	$PackageFullName = (Get-AppxPackage -AllUsers $App).PackageFullName
	if ($PackageFullName) {
		Write-Host "Removing Package: $PackageFullName"
		# Note: As of W10 20H2, the -AllUsers flag doesn't work for a large number of the apps in the list.
		# Remove-AppxPackage -AllUsers -package $PackageFullName
		Remove-AppxPackage -package $PackageFullName
	}
	else {
		Write-Host "Unable to find package: $App"
	}
}
