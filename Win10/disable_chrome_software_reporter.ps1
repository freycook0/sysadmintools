# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack to disable the software reporter in Google Chrome.  The idea is to provide a tool to change this setting after a fresh install of Windows.
# Original source: https://www.techworm.net/2020/02/disable-software-reporter-tool.html
# Original source: https://www.reddit.com/r/sysadmin/comments/fgvyju/microsoft_edge_browser_is_more_privacyinvading/fk8a3we/
# Use Case: To be run each time a new user is added to a machine, and once on each machine after an initial install by an account with local admin privileges.  This script will automatically update all user profiles on the machine.  The change will take effect after a restart.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Begin with the machine-based registry changes, then proceed with enumerating and editing all user profiles.

# Machine-level policies that Chrome checks to decide if it should run the cleanup or cleanup reporting tools.
[Microsoft.Win32.Registry]::SetValue('HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Chrome', 'ChromeCleanupEnabled', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
[Microsoft.Win32.Registry]::SetValue('HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Chrome', 'ChromeCleanupReportingEnabled', 0, [Microsoft.Win32.RegistryValueKind]::DWord)

# Regular Expressions to select certain users/profiles (or to filter out unwanted users/profiles).
$UsersRegex = '' # For Example, you could use '(User\d\d|MyUser)$'
$Regex = "C:\\Users\\$UsersRegex"
$UserProfiles = @(Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\*" | Where ProfileImagePath -match "$Regex" | Select ProfileImagePath, PSChildName)
$tmp = New-Object psobject -Property @{
	ProfileImagePath='C:\Users\Default'
	PSChildName='.DEFAULT'
}
$UserProfiles += $tmp

# Temporary registry path to load profiles to (if necessary).
$TempHKUPath = 'HKU:\TEMP'
$TempHKUsersPath = 'HKEY_USERS\TEMP'

# Load HKEY_USERS Registry
New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS

Foreach ($UserProfile in $UserProfiles){
	# Query HKU to see if user registry is already loaded.  If so, update it right then and there.  Otherwise, load it manually to TEMP, update it in TEMP, then unload it manually.
	$ProfilePath = $UserProfile.ProfileImagePath
	$sid = $UserProfile.PSChildName
	$HKUPath = "HKU:\$sid"
	$HKUsersPath = "HKEY_USERS\$sid"
	
	$RegPath = ''
	$ManualLoad = $false
	Get-ItemProperty -Path $HKUPath
	if($?){
		# Registry is already loaded; we're good to go!
		$RegPath = $HKUsersPath
	}
	else{
		# Registry isn't loaded; we need to load it manually (and unload it later).
		$ManualLoad = $true
		$db = "$ProfilePath\NTUser.dat"
		reg load $TempHKUsersPath $db
		$RegPath = $TempHKUsersPath
	}
	
	# User-level policy that tells Windows not to allow the software reporter in Chrome to run.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun", '1', 'software_reporter_tool.exe', [Microsoft.Win32.RegistryValueKind]::String)
	
	# Pull out the big guns and clear out software_reporter_tool.exe in each user profile (as many times as you have to).
	$sw_r_files = Get-ChildItem "$ProfilePath\AppData\Local\Google\Chrome\User Data\SwReporter\*\software_reporter_tool.exe"
	ForEach($sw_r_file in $sw_r_files){
		$sw_r_fullpath = $sw_r_file.FullName
		if(Test-Path $sw_r_fullpath){
			echo '' > $sw_r_fullpath
		}
	}
	
	# If we manually loaded the user's registry, let's go ahead and unload it.
	if($ManualLoad){
		reg unload $TempHKUsersPath
	}
}
