# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack to disable clipboard history in Windows 10.  There can be security concerns around clipboard history leaking sensitive info, such as passwords.  The idea is to provide a tool to change this setting after a fresh install of Windows.
# Original source: https://www.tenforums.com/tutorials/110039-enable-disable-clipboard-history-windows-10-a.html#option2
# Use Case: To be run once on each machine.  A reboot is required for the change to take effect.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Adding the registry key, setting it to a value of 0.  If the registry key does not exist already in the system, then it will be created.
[Microsoft.Win32.Registry]::SetValue('HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\System', 'AllowClipboardHistory', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
