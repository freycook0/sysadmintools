<#
 .Synopsis
	Maps a Path to a Physical Disk.

 .Description
	Maps a Path to a Physical Disk.  This function takes a path as a argument and returns some basic information on the physical disk that the location is based on.

 .Parameter locationString
	The Path to be mapped from.

 .Example
	# Map the current path to its physical disk.
	Map-PathToPhysicalDisk

 .Example
	# Map the user's home directory to its physical disk.
	Map-PathToPhysicalDisk ~

 .Example
	# Map a specific path to its physical disk.
	Map-PathToPhysicalDisk 'Z:\Some Path\to\a\specific\directory\'
#>
function Map-PathToPhysicalDisk{
	param(
		[string] $LocationString = '.\'
	)
	# $locationString = 'C:\Users\'
	# $locationString = $Args[0]
	$location = Get-Item $locationString
	$root = $location.Root.Name.Substring(0,1)
	$partition = Get-Partition | Where DriveLetter -eq $root
	$physicaldisk = Get-PhysicalDisk | Where DeviceId -eq $partition.DiskNumber
	# $physicaldiskType = $physicaldisk.MediaType
	# $physicaldiskConn = $physicaldisk.BusType
	return $physicaldisk
}
Export-ModuleMember -Function Map-PathToPhysicalDisk
