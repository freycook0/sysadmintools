# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack for screen saver settings, with the ultimate goal of setting a lockout timer.  The idea is to provide a tool to change this setting after a fresh install of Windows.
# Use Case: To be run once on each machine by an account with local admin privileges.  This script will automatically update all user profiles on the machine.  The change will take effect after a reboot.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Regular Expressions to filter out unwanted users/profiles.
$UsersRegex = '' # For Example, you could use '(User\d\d|MyUser)$'
$Regex = "C:\\Users\\$UsersRegex"
$UserProfiles = @(Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\*" | Where ProfileImagePath -match "$Regex" | Select ProfileImagePath, PSChildName)
$tmp = New-Object psobject -Property @{
	ProfileImagePath='C:\Users\Default'
	PSChildName='.DEFAULT'
}
$UserProfiles += $tmp

# Temporary registry path to load profiles to (if necessary).
$TempHKUPath = 'HKU:\TEMP'
$TempHKUsersPath = 'HKEY_USERS\TEMP'

# Load HKEY_USERS Registry
New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS

Foreach ($UserProfile in $UserProfiles){
	# Query HKU to see if user registry is already loaded.  If so, update it right then and there.  Otherwise, load it manually to TEMP, update it in TEMP, then unload it manually.
	$ProfilePath = $UserProfile.ProfileImagePath
	$sid = $UserProfile.PSChildName
	$HKUPath = "HKU:\$sid"
	$HKUsersPath = "HKEY_USERS\$sid"
	$RegPath = ''
	$ManualLoad = $false
	Get-ItemProperty -Path $HKUPath
	if($?){
		# Registry is already loaded; we're good to go!
		$RegPath = $HKUsersPath
	}
	else{
		# Registry isn't loaded; we need to load it manually (and unload it later).
		$ManualLoad = $true
		$db = "$ProfilePath\NTUser.dat"
		reg load $TempHKUsersPath $db
		$RegPath = $TempHKUsersPath
	}
	
	# Settings -> Privacy -> App Permissions -> File system
	
	# Test if the blank screensaver is in the System32 folder.  If it's there, proceed; otherwise, error out.
	# $ScreenSaver = 'C:\Windows\System32\scrnsave.scr'
	$ScreenSaver = "$env:windir\System32\scrnsave.scr"
	If(Test-Path $ScreenSaver){
		# Set the screensaver to active.
		[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Desktop", 'ScreenSaveActive', '1', [Microsoft.Win32.RegistryValueKind]::String)
		# Set the screensaver to lock the computer after it activates.  This is the main goal for this script.
		[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Desktop", 'ScreenSaverIsSecure', '1', [Microsoft.Win32.RegistryValueKind]::String)
		# Set the timeout for the screensaver.  The value is the number of seconds it takes for the screensaver to activate.
		[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Desktop", 'ScreenSaveTimeOut', '540', [Microsoft.Win32.RegistryValueKind]::String)
		# Indicate that the blank (black screen) screensaver should be used.
		[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Desktop", 'SCRNSAVE.EXE', $ScreenSaver, [Microsoft.Win32.RegistryValueKind]::String)
	}
	Else{
		Write-Host 'Unable to find scrnsave.scr in the System32 folder.  Skipping Screen Saver settings.'
	}
	
	# If we manually loaded the user's registry, let's go ahead and unload it.
	if($ManualLoad){
		reg unload $TempHKUsersPath
	}
}
