## SysAdminTools

Howdy!  Thanks for checking out SysAdminTools; a collection of tools aimed at helping sysadmins everywhere with various tasks in managing a Windows-based environment, personal or enterprise.  Some of the tools you will find here:
- Win10: A series of scripts aimed at automating various steps the preparation of new Windows machines and images.
- More to come!

I try to include copious documentation in the scripts themselves, as the idea is to not only provide tools for sysadmins, but also to provide education for anyone unfamiliar with the material.  I hope you find this collection helpful!

*-James*
