# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack to turn off Cortana and the web-based Windows Search.  The idea is to provide a tool to change this setting after a fresh install of Windows.
# Original source: https://www.reddit.com/r/sysadmin/comments/ez92x0/windows_10_no_results_in_search_window/fglqod1/
# Use Case: To be run once on each machine.  A reboot is required for the change to take effect.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Begin with the machine-based registry changes, then proceed with enumerating and editing all user profiles.

# Disable Cortana.  Adding to the registry key, setting it to a value of 0.  If the value does not exist already in the registry key, then it will be created.
[Microsoft.Win32.Registry]::SetValue('HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsSearch', 'AllowCortana', 0, [Microsoft.Win32.RegistryValueKind]::DWord)

# Disable searching the web (Bing) in the search box.  Adding to the registry key, setting it to a value of 1.  If the value does not exist already in the registry key, then it will be created.
[Microsoft.Win32.Registry]::SetValue('HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsSearch', 'DisableWebSearch', 1, [Microsoft.Win32.RegistryValueKind]::DWord)

# Regular Expressions to select certain users/profiles (or to filter out unwanted users/profiles).
$UsersRegex = '' # For Example, you could use '(User\d\d|MyUser)$'
$Regex = "C:\\Users\\$UsersRegex"
$UserProfiles = @(Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\*" | Where ProfileImagePath -match "$Regex" | Select ProfileImagePath, PSChildName)
$tmp = New-Object psobject -Property @{
	ProfileImagePath='C:\Users\Default'
	PSChildName='.DEFAULT'
}
$UserProfiles += $tmp

# Temporary registry path to load profiles to (if necessary).
$TempHKUPath = 'HKU:\TEMP'
$TempHKUsersPath = 'HKEY_USERS\TEMP'

# Load HKEY_USERS Registry
New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS

Foreach ($UserProfile in $UserProfiles){
	# Query HKU to see if user registry is already loaded.  If so, update it right then and there.  Otherwise, load it manually to TEMP, update it in TEMP, then unload it manually.
	$ProfilePath = $UserProfile.ProfileImagePath
	$sid = $UserProfile.PSChildName
	$HKUPath = "HKU:\$sid"
	$HKUsersPath = "HKEY_USERS\$sid"
	
	$RegPath = ''
	$ManualLoad = $false
	Get-ItemProperty -Path $HKUPath
	if($?){
		# Registry is already loaded; we're good to go!
		$RegPath = $HKUsersPath
	}
	else{
		# Registry isn't loaded; we need to load it manually (and unload it later).
		$ManualLoad = $true
		$db = "$ProfilePath\NTUser.dat"
		reg load $TempHKUsersPath $db
		$RegPath = $TempHKUsersPath
	}
	
	# Disable Cortana.  Adding to the registry key, setting it to a value of 0.  If the value does not exist already in the registry key, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Search", 'CortanaConsent', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	
	# Disable searching the web (Bing) in the search box.  Adding to the registry key, setting it to a value of 0.  If the value does not exist already in the registry key, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Search", 'BingSearchEnabled', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	
	# If we manually loaded the user's registry, let's go ahead and unload it.
	if($ManualLoad){
		reg unload $TempHKUsersPath
	}
}
