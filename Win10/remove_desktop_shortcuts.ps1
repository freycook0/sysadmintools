# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script deletes shotrcuts from the desktop of all users, the desktop of the default profile, and the public desktop.  The reasoning is that some program installs do not provide an option to skip the desktop shortcut, and not everyone likes desktop shortcuts. The idea is to provide a tool to perform this cleanup after the installation of all desired prorams after a fresh install of Windows.
# Use Case: To be run once on each machine by an account with local admin privileges.  This script will automatically update all desktops on the machine.  The change will take effect immediately.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Regular Expressions to filter out unwanted users/profiles.
$UsersRegex = '' # For Example, you could use '(User\d\d|MyUser)$'
$Regex = "C:\\Users\\$UsersRegex"
$UserProfiles = @(Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\*" | Where ProfileImagePath -match "$Regex" | Select ProfileImagePath)
$tmp = New-Object psobject -Property @{
	ProfileImagePath='C:\Users\Default'
}
$UserProfiles += $tmp
$tmp = New-Object psobject -Property @{
	ProfileImagePath='C:\Users\Public'
}
$UserProfiles += $tmp

Foreach ($UserProfile in $UserProfiles){
	$tmp = ($UserProfile.ProfileImagePath)
	Get-ChildItem "$tmp\Desktop\*.lnk" | Remove-Item
}
