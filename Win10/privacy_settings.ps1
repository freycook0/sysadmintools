# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack for several privacy-related settings. The idea is to provide a tool to change this setting after a fresh install of Windows.
# Use Case: To be run once on each machine by an account with local admin privileges.  This script will automatically update all user profiles on the machine.  The change will take effect on the next logon for each user.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Regular Expressions to filter out unwanted users/profiles.
$UsersRegex = '' # For Example, you could use '(User\d\d|MyUser)$'
$Regex = "C:\\Users\\$UsersRegex"
$UserProfiles = @(Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\*" | Where ProfileImagePath -match "$Regex" | Select ProfileImagePath, PSChildName)
$tmp = New-Object psobject -Property @{
	ProfileImagePath='C:\Users\Default'
	PSChildName='.DEFAULT'
}
$UserProfiles += $tmp

# Temporary registry path to load profiles to (if necessary).
$TempHKUPath = 'HKU:\TEMP'
$TempHKUsersPath = 'HKEY_USERS\TEMP'

# Load HKEY_USERS Registry
New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS

Foreach ($UserProfile in $UserProfiles){

	# Query HKU to see if user registry is already loaded.  If so, update it right then and there.  Otherwise, load it manually to TEMP, update it in TEMP, then unload it manually.
	$ProfilePath = $UserProfile.ProfileImagePath
	$sid = $UserProfile.PSChildName
	$HKUPath = "HKU:\$sid"
	$HKUsersPath = "HKEY_USERS\$sid"

	$RegPath = ''
	$ManualLoad = $false
	Get-ItemProperty -Path $HKUPath
	if($?){
		# Registry is already loaded; we're good to go!
		$RegPath = $HKUsersPath
	}
	else{
		# Registry isn't loaded; we need to load it manually (and unload it later).
		$ManualLoad = $true
		$db = "$ProfilePath\NTUser.dat"
		reg load $TempHKUsersPath $db
		$RegPath = $TempHKUsersPath
	}

	# Settings -> Privacy -> Windows Permissions -> General

	# "Let apps use advertising ID to make ads more interesting to you based on your app usage (turning this off will reset your ID)"
	# Disabling Unique ID for Advertising, then deleting the value with the unique ID, if it exists.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\AdvertisingInfo", 'Enabled', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	# Remove-ItemProperty -path "$RegPath\Software\Microsoft\Windows\CurrentVersion\AdvertisingInfo" -Name "Id" -Force
	$tmp = [Microsoft.Win32.Registry]::Users.OpenSubKey("$sid\Software\Microsoft\Windows\CurrentVersion\AdvertisingInfo", $true)
	# Attemp to delete the value 'Accept Language'.  If the value does not exist, then suppress the error message.
	try{
		$tmp.DeleteValue('Id')
	} catch {
		if($PSItem.ToString() -ne 'Exception calling "DeleteValue" with "1" argument(s): "No value exists with that name."'){
			throw $PSItem
		}
	}

	# "Let websites provide locally relevant content by accessing my language list"
	# Opting Out of sharing languages through IE
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\International\User Profile", 'HttpAcceptLanguageOptOut', 1, [Microsoft.Win32.RegistryValueKind]::DWord)
	# Remove-ItemProperty "$RegPath\Software\Microsoft\Internet Explorer\International" -Name "AcceptLanguage" -Force
	$tmp = [Microsoft.Win32.Registry]::Users.OpenSubKey("$sid\Software\Microsoft\Internet Explorer\International", $true)
	# Attemp to delete the value 'Accept Language'.  If the value does not exist, then suppress the error message.
	try{
		$tmp.DeleteValue('AcceptLanguage')
	} catch {
		if($PSItem.ToString() -ne 'Exception calling "DeleteValue" with "1" argument(s): "No value exists with that name."'){
			throw $PSItem
		}
	}

	# "Let Windows track app launches to improve Start and search results"
	# Turning off app launch tracking
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced", 'Start_TrackProgs', 0, [Microsoft.Win32.RegistryValueKind]::DWord)

	# "Show me suggested content in the Settings app"
	# Turning off suggested content in Settings App
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", 'SubscribedContent-338393Enabled', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", 'SubscribedContent-353694Enabled', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", 'SubscribedContent-353696Enabled', 0, [Microsoft.Win32.RegistryValueKind]::DWord)

	# Settings -> Privacy -> Windows Permissions -> Speech, inking, & typing

	# Settings -> Privacy -> Windows Permissions -> Diagnostics & feedback

	# "Improve inking & typing recognition"
	# "Send inking and typing data to Microsoft to improve the language recognition and suggestion capabilitys of apps and services running on Windows.
	# Disable inking & typing recognition
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Input\TIPC", 'Enabled', 0, [Microsoft.Win32.RegistryValueKind]::DWord)

	# "Tailored Experiences"
	# "Let Microsoft offer you tailored experiences based on the diagnostic data setting you have chosen. Tailored experiences are personalized tips, ads, and recommendations that enhance Microsoft products and services for your needs"
	# Disabled Tailored experiences
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Privacy", 'TailoredExperiencesWithDiagnosticDataEnabled', 0, [Microsoft.Win32.RegistryValueKind]::DWord)

	# Settings -> Privacy -> Windows Permissions -> Activity History

	# Settings -> Privacy -> App Permissions -> Location

	# Settings -> Privacy -> App Permissions -> Camera

	# Settings -> Privacy -> App Permissions -> Microphone

	# Settings -> Privacy -> App Permissions -> Notifications

	# Disallow notifications access to apps
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\{52079E78-A92B-413F-B213-E8FE35712E72}", 'Value', 'Deny', [Microsoft.Win32.RegistryValueKind]::String)

	# Settings -> Privacy -> App Permissions -> Account info

	# Settings -> Privacy -> App Permissions -> Contacts

	# Settings -> Privacy -> App Permissions -> Calendar

	# Settings -> Privacy -> App Permissions -> Call history

	# Settings -> Privacy -> App Permissions -> Email

	# Settings -> Privacy -> App Permissions -> Tasks

	# Settings -> Privacy -> App Permissions -> Messaging

	# Settings -> Privacy -> App Permissions -> Radios

	# Disallow radio access to apps
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\{A8804298-2D5F-42E3-9531-9C8C39EB29CE}", 'Value', 'Deny', [Microsoft.Win32.RegistryValueKind]::String)

	# Settings -> Privacy -> App Permissions -> Other devices

	# Disallow unpaired device access to apps
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\LooselyCoupled", 'Value', 'Deny', [Microsoft.Win32.RegistryValueKind]::String)

	# Settings -> Privacy -> App Permissions -> Background apps

	# Disallow apps running in the background
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Search", 'BackgroundAppGlobalToggle', 0, [Microsoft.Win32.RegistryValueKind]::DWord)

	# Settings -> Privacy -> App Permissions -> App diagnostics

	# Disallow diagnostics access to apps
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\{2297E4E2-5DBE-466D-A12B-0F8286F0D9CA}", 'Value', 'Deny', [Microsoft.Win32.RegistryValueKind]::String)

	# Settings -> Privacy -> App Permissions -> Automatics file downloads

	# Settings -> Privacy -> App Permissions -> Documents

	# Settings -> Privacy -> App Permissions -> Pictures

	# Settings -> Privacy -> App Permissions -> Videos

	# Settings -> Privacy -> App Permissions -> File system

	# If we manually loaded the user's registry, let's go ahead and unload it.
	if($ManualLoad){
		reg unload $TempHKUsersPath
	}
}
