# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack to supress the notification telling you that not all the network drives are connected.  The idea is to provide a tool to change this setting after a fresh install of Windows.
# Original source: https://winaero.com/blog/disable-could-not-reconnect-all-network-drives-notification-in-windows-10/
# Use Case: To be run once on each machine.  A reboot is required for the change to take effect.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Adding the registry key, setting it to a value of 0.  If the registry key does not exist already in the system, then it will be created.
[Microsoft.Win32.Registry]::SetValue('HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\NetworkProvider', 'RestoreConnection', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
