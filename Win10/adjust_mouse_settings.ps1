# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack to adjust mouse settings in the Control Panel.  The idea is to provide a tool to change this setting after a fresh install of Windows.
# Please note that the mouse settings below were decided for the author's personal preference, and are intended to be adjusted to fit the user's personal preference.
# Use Case: To be run once on each machine by an account with local admin privileges.  This script will automatically update all user profiles on the machine.  The change will take effect on the next logon for each user.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-not $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Regular Expressions to filter out unwanted users/profiles.
$UsersRegex = '' # For Example, you could use '(User\d\d|MyUser)$'
$Regex = "C:\\Users\\$UsersRegex"
$UserProfiles = @(Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\*" | Where ProfileImagePath -match "$Regex" | Select ProfileImagePath, PSChildName)
$tmp = New-Object psobject -Property @{
	ProfileImagePath='C:\Users\Default'
	PSChildName='.DEFAULT'
}
$UserProfiles += $tmp

# Temporary registry path to load profiles to (if necessary).
$TempHKUPath = 'HKU:\TEMP'
$TempHKUsersPath = 'HKEY_USERS\TEMP'

# Load HKEY_USERS Registry
New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS

Foreach ($UserProfile in $UserProfiles){
	# Query HKU to see if user registry is already loaded.  If so, update it right then and there.  Otherwise, load it manually to TEMP, update it in TEMP, then unload it manually.
	$ProfilePath = $UserProfile.ProfileImagePath
	$sid = $UserProfile.PSChildName
	$HKUPath = "HKU:\$sid"
	$HKUsersPath = "HKEY_USERS\$sid"
	
	$RegPath = ''
	$ManualLoad = $false
	Get-ItemProperty -Path $HKUPath
	if($?){
		# Registry is already loaded; we're good to go!
		# $RegPath = $HKUPath
		$RegPath = $HKUsersPath
	}
	else{
		# Registry isn't loaded; we need to load it manually (and unload it later).
		$ManualLoad = $true
		$db = "$ProfilePath\NTUser.dat"
		reg load $TempHKUsersPath $db
		# $RegPath = $TempHKUPath
		$RegPath = $TempHKUsersPath
	}
	
	# Control Panel -> Hardware and Sound -> Mouse -> Pointer Options -> Visibility-> Display pointer trails
	# Turning off the mouse trail.  Adding to the registry key, setting it to a value of 0.  If the registry key does not exist already in the system, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Mouse", 'MouseTrails', '0', [Microsoft.Win32.RegistryValueKind]::String)
	
	# Control Panel -> Hardware and Sound -> Mouse -> Pointer Options -> Motion -> Enhance pointer precision
	# Turning off the "Enhance pointer precision" setting.  Adding to the registry keys, setting them to 0.  If the registry key does not exist already in the system, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Mouse", 'MouseThreshold1', '0', [Microsoft.Win32.RegistryValueKind]::String)
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Mouse", 'MouseThreshold2', '0', [Microsoft.Win32.RegistryValueKind]::String)
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Mouse", 'MouseSpeed', '0', [Microsoft.Win32.RegistryValueKind]::String)
	
	# Control Panel -> Hardware and Sound -> Mouse -> Pointer Options -> Motion -> Select a pointer speed:
	# Setting mouse sensitivity.  A value of 10 is right in the middle of the slider.  Each increment in the slider is an increase of 2.  Adding to the registry key, setting it to a value of 10.  If the registry key does not exist already in the system, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Mouse", 'MouseSensitivity', '10', [Microsoft.Win32.RegistryValueKind]::String)
	
	# Control Panel -> Hardware and Sound -> Mouse -> Wheel -> Horizontal Scrolling -> Tilt the wheel to scroll the following number of characters at a time:
	# Control Panel -> Hardware and Sound -> Mouse -> Wheel -> Vertical Scrolling -> Roll the wheel one notch to scroll:
	# Setting Wheel Scrolls, for both lines and characters.  A value of 6 is about double the default for Windows.  Adding to the registry key, setting it to a value of 6.  If the registry key does not exist already in the system, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Desktop", 'WheelScrollChars', '6', [Microsoft.Win32.RegistryValueKind]::String)
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Desktop", 'WheelScrollLines', '6', [Microsoft.Win32.RegistryValueKind]::String)
	
	# If we manually loaded the user's registry, let's go ahead and unload it.
	if($ManualLoad){
		reg unload $TempHKUsersPath
	}
}
