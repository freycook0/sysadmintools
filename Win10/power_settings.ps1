# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as documentation on power settings in Windows, and as a tool to change (most of) them.  The idea is to provide a tool to set power settings just the way you like them after a fresh install of Windows.
# Documentation:
# https://docs.microsoft.com/en-us/windows-hardware/design/device-experiences/powercfg-command-line-options
# https://ss64.com/nt/powercfg.html
# Use Case: To be run once on each machine.  A reboot is required for the change to take effect.

# How to pull up this info, as well as the current power plan:
# powercfg.exe /list
# powercfg.exe /getactivescheme
# powercfg.exe /query 381b4222-f694-41f0-9685-ff5bb260df2e
# Note: 381b4222-f694-41f0-9685-ff5bb260df2e seems to be the default/built-in/out-of-the-box "Balanced" power plan in Win10.

# ---------------------------------------------------
# Power Settings Documentation
# ---------------------------------------------------

# Wireless Adapter Settings (on my Win10 device)
# 19cbb8fa-5279-450e-9fac-8a3d5fedd0c1

# Power Saving Mode
# 12bbebe6-58d6-4636-95bb-3217ef867c1a
# 000 Maximum Performance
# 001 Low Power Saving
# 002 Medium Power Saving
# 003 Maximum Power Saving

# Sleep
# 238c9fa8-0aad-41ed-83f4-97be242c8f20
# SUB_SLEEP

# Sleep after
# 29f6c1db-86da-48c5-9fdb-f2b67b1f44da
# STANDBYIDLE
# 0x00000000 0 seconds (do not sleep)
# 0x00000001 1 second
# 0xffffffff 4294967295 seconds

# Allow hybrid sleep
# 94ac6d29-73ce-41a6-809f-6363ba21b47e
# HYBRIDSLEEP
# 000 Off
# 001 On

# Hibernate after
# 9d7815a6-7ee4-497e-8888-515a05f02364
# HIBERNATEIDLE
# 0x00000000 0 seconds (do not hibernate)
# 0x00000001 1 second
# 0xffffffff 4294967295 seconds

# Allow wake timers
# 94ac6d29-73ce-41a6-809f-6363ba21b47e
# RTCWAKE
# 000 Disable
# 001 Enable
# 002 Important Wake Timers Only

# Power buttons and lid
# 4f971e89-eebd-4455-a8de-9e59040e7347
# SUB_BUTTONS

# Lid close action
# 5ca83367-6e45-459f-a27b-476b1d01c936
# LIDACTION
# 000 Do nothing
# 001 Sleep
# 002 Hibernate
# 003 Shut down

# Power button action
# 7648efa3-dd9c-4e3e-b566-50f929386280
# PBUTTONACTION
# 000 Do nothing
# 001 Sleep
# 002 Hibernate
# 003 Shut down
# 004 Turn off the display

# Sleep button action
# 96996bc0-ad50-47ec-923b-6f41874dd9eb
# SBUTTONACTION
# 000 Do nothing
# 001 Sleep
# 002 Hibernate
# 003 Shut down
# 004 Turn off the display

# Start menu power button
# a7066653-8d6c-40a8-910e-a1f54b84c7e5
# UIBUTTON_ACTION
# 000 Do nothing
# 001 Sleep
# 002 Hibernate
# 003 Shut down

# Display
# 7516b95f-f776-4464-8c53-06167f40cc99
# SUB_VIDEO

# Turn off display after
# 3c0bc021-c8a8-4e07-a973-6b14cbcb2b7e
# VIDEOIDLE
# 0x00000000 0 seconds (do not turn off)
# 0x00000001 1 second
# 0xffffffff 4294967295 seconds

# Display brightness
# aded5e82-b909-4619-9949-f5d71dac0bcb
# 0x00000000 0%
# 0x00000064 100%

# Dimmed display brightness
# f1fbfde2-a960-4165-9f88-50667911ce96
# 0x00000000 0%
# 0x00000064 100%

# Enable adaptive brightness
# fbd9aa66-9553-4097-ba44-ed6e9d65eab8
# ADAPTBRIGHT
# 000 Off
# 001 On

# Battery
# e73a048d-bf27-4f12-9731-8b2076e8891f
# SUB_BATTERY

# Critical battery level
# 9a66d8d7-4ff7-4ef9-b5a2-5a326ca2a469
# BATLEVELCRIT
# 0x00000000 0%
# 0x00000064 100%

# Critical battery action
# 637ea02f-bbcb-4015-8e2c-a1c7b9c0b546
# BATACTIONCRIT
# 000 Do nothing
# 001 Sleep
# 002 Hibernate
# 003 Shut down

# Low battery level
# 8183ba9a-e910-48da-8769-14ae6dc1170a
# BATLEVELLOW
# 0x00000000 0%
# 0x00000064 100%

# Low battery action
# d8742dcb-3e6a-4b3c-b3fe-374623cdcf06
# BATACTIONLOW
# 000 Do nothing
# 001 Sleep
# 002 Hibernate
# 003 Shut down

# Low battery notification
# bcded951-187b-4d05-bccc-f7e51960c258
# BATFLAGSLOW
# 000 Off
# 001 On

# Reserve battery level
# f3c5027d-cd16-4930-aa6b-90db844a8f00
# 0x00000000 0%
# 0x00000064 100%
# apparently this is currently 7% on my Win10 device.  Don't know if this is useful/usable or not.

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Balanced power plan		Wireless Adapter Settings		Power Saving Mode		plugged in		0 seconds
powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e 19cbb8fa-5279-450e-9fac-8a3d5fedd0c1 12bbebe6-58d6-4636-95bb-3217ef867c1a 000
# Balanced power plan		Wireless Adapter Settings		Power Saving Mode		battery power		0 seconds
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e 19cbb8fa-5279-450e-9fac-8a3d5fedd0c1 12bbebe6-58d6-4636-95bb-3217ef867c1a 000

# Balanced power plan		Sleep		Sleep after		plugged in		0 seconds
# powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_SLEEP STANDBYIDLE 0x00000000 # This command fails every time.  Research has identified no methods of programmatically setting the "Sleep After" setting to "Never".  This still needs to be set manually after every install.
# Balanced power plan		Sleep		Sleep after		battery power		0 seconds
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_SLEEP STANDBYIDLE 0x00000000

# Balanced power plan		Sleep		Hibernate after		plugged in		0 seconds
# powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_SLEEP HIBERNATEIDLE 0x00000000 # This failed on my Win10 device
# Balanced power plan		Sleep		Hibernate after		battery power		0 seconds
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_SLEEP HIBERNATEIDLE 0x00000000

# Balanced power plan		Sleep		Allow hybrid sleep		plugged in		Off
powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_SLEEP HYBRIDSLEEP 000
# Balanced power plan		Sleep		Allow hybrid sleep		battery power		Off
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_SLEEP HYBRIDSLEEP 000

# Balanced power plan		Power buttons and lid		Lid close action		plugged in Do nothing
powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BUTTONS LIDACTION 000
# Balanced power plan		Power buttons and lid		Lid close action		battery power Do nothing
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BUTTONS LIDACTION 000

# Balanced power plan		Power buttons and lid		Power button action		plugged in		Shut down
powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BUTTONS PBUTTONACTION 003
# Balanced power plan		Power buttons and lid		Power button action		battery power		Shut down
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BUTTONS PBUTTONACTION 003

# Balanced power plan		Power buttons and lid		Sleep button action		plugged in		Sleep
powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BUTTONS SBUTTONACTION 001
# Balanced power plan		Power buttons and lid		Sleep button action		battery power		Sleep
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BUTTONS SBUTTONACTION 001

# Balanced power plan		Power buttons and lid		Start menu power button		plugged in		Shut down
# powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BUTTONS UIBUTTON_ACTION 003 # This failed on my Win10 device
# Balanced power plan		Power buttons and lid		Start menu power button		battery power		Shut down
# powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BUTTONS UIBUTTON_ACTION 003 # This failed on my Win10 device

# Balanced power plan		Display		Turn off display after		plugged in		0 seconds
# powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_VIDEO VIDEOIDLE 0x00000000 # This failed on my Win10 device
# Balanced power plan		Display		Turn off display after		battery power		1200 seconds (20 min)
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_VIDEO VIDEOIDLE 0x000004B0

# Balanced power plan		Display		Display brightness		plugged in		100%
# powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_VIDEO aded5e82-b909-4619-9949-f5d71dac0bcb 0x00000064 # This failed on my Win10 device
# Balanced power plan		Display		Display brightness		battery power		100%
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_VIDEO aded5e82-b909-4619-9949-f5d71dac0bcb 0x00000064

# I don't think these work if the adaptive brightness is disabled
# Balanced power plan		Display		Dimmed display brightness		plugged in		100%
# powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_VIDEO f1fbfde2-a960-4165-9f88-50667911ce96 0x00000064
# Balanced power plan		Display		Dimmed display brightness		battery power		80%
# powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_VIDEO f1fbfde2-a960-4165-9f88-50667911ce96 0x00000050

# Balanced power plan		Display		Enable adaptive brightness		plugged in		Off
powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_VIDEO ADAPTBRIGHT 000
# Balanced power plan		Display		Enable adaptive brightness		battery power		On
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_VIDEO ADAPTBRIGHT 001

# Balanced power plan		Battery		Critical battery level		plugged in		3%
# powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BATTERY BATLEVELCRIT 0x00000003 # This failed on my Win10 device
# Balanced power plan		Battery		Critical battery level		battery power		3%
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BATTERY BATLEVELCRIT 0x00000003

# Balanced power plan		Battery		Critical battery action		plugged in		Shut down
powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BATTERY BATACTIONCRIT 003
# Balanced power plan		Battery		Critical battery action		battery power		Shut down
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BATTERY BATACTIONCRIT 003

# Balanced power plan		Battery		Low battery level		plugged in		10%
# powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BATTERY BATLEVELLOW 0x0000000F # This failed on my Win10 device
# Balanced power plan		Battery		Low battery level		battery power		10%
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BATTERY BATLEVELLOW 0x0000000F

# Balanced power plan		Battery		Low battery action		plugged in		Do nothing
powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BATTERY BATACTIONLOW 000
# Balanced power plan		Battery		Low battery action		battery power		Do nothing
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BATTERY BATACTIONLOW 000

# Balanced power plan		Battery		Low battery notification		plugged in		On
powercfg.exe /setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BATTERY BATACTIONLOW 001
# Balanced power plan		Battery		Low battery notification		battery power		On
powercfg.exe /setdcvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e SUB_BATTERY BATACTIONLOW 001

# A restart may be needed after for changes to take effect.
