# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script edits the layout xml file in AppData to "unsticky" all items that are automatically pinned to the taskbar.  The idea is to provide a tool to remove these automatically ("sticky") pinned items from the taskbar after a fresh install of Windows, or after a major Windows update.
# Use Case: To be run once on each machine by an account with local admin privileges.  This script will automatically update all user profiles on the machine.  The change will take effect on the next logon for each user.
# Note: This script can also be modified to update the default profile, so it doesn't have to be run each time a new profile is created/added.

# ---------------------------------------------------
# Taskbar Layout Examples
# ---------------------------------------------------

# Example layout xml file after a fresh install or a major update

# <?xml version="1.0" encoding="utf-8"?>
# <LayoutModificationTemplate xmlns="https://schemas.microsoft.com/Start/2014/LayoutModification" xmlns:defaultlayout="http://schemas.microsoft.com/Start/2014/FullDefaultLayout" xmlns:start="http://schemas.microsoft.com/Start/2014/TaskbarLayout" Version="1">
#   <LayoutOptions StartTileGroupCellWidth="6" StartTilegroupsColumnCount="1" />
#   <DefaultLayoutOverride>
#     <StartLayoutCollection>
#       <defaultlayout:StartLayout GroupCellWidth="6">
#         <start:Group Name="Play">
#           <start:Tile Size="1x1" Column="2" Row="0" AppUserModelID="Microsoft.WindowsCalculator_8wekyb3d8bbwe!App" />
#         </start:Group>
#       </defaultlayout:StartLayout>
#     </StartLayoutCollection>
#   </DefaultLayoutOverride>
#   <CustomTaskbarLayoutCollection PinListPlacement="Replace">
#     <defaultlayout:TaskbarLayout>
#       <TaskbarPinList>
#         <DesktopApp DesktopApplicationLinkPath="%APPDATA%\Microsoft\Windows\Start Menu\Programs\Accessories\Internet Explorer.lnk" />
#         <UWA AppUserModelID="Microsoft.Office.excel_8wekyb3d8bbwe!microsoft.excel" />
#         <UWA AppUserModelID="Microsoft.Office.Word_8wekyb3d8bbwe!microsoft.word" />
#       </TaskbarPinList>
#     </defaultlayout:TaskbarLayout>
#   </CustomTaskbarLayoutCollection>
# </LayoutModificationTemplate>

# Example layout xml file after the script runs

# <?xml version="1.0" encoding="utf-8"?>
# <LayoutModificationTemplate xmlns="https://schemas.microsoft.com/Start/2014/LayoutModification" xmlns:defaultlayout="http://schemas.microsoft.com/Start/2014/FullDefaultLayout" xmlns:start="http://schemas.microsoft.com/Start/2014/TaskbarLayout" Version="1">
#   <LayoutOptions StartTileGroupCellWidth="6" StartTilegroupsColumnCount="1" />
#   <DefaultLayoutOverride>
#     <StartLayoutCollection>
#       <defaultlayout:StartLayout GroupCellWidth="6">
#         <start:Group Name="Play">
#           <start:Tile Size="1x1" Column="2" Row="0" AppUserModelID="Microsoft.WindowsCalculator_8wekyb3d8bbwe!App" />
#         </start:Group>
#       </defaultlayout:StartLayout>
#     </StartLayoutCollection>
#   </DefaultLayoutOverride>
#   <CustomTaskbarLayoutCollection PinListPlacement="Replace">
#     <defaultlayout:TaskbarLayout>
#       <TaskbarPinList>
#       </TaskbarPinList>
#     </defaultlayout:TaskbarLayout>
#   </CustomTaskbarLayoutCollection>
# </LayoutModificationTemplate>

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Regular Expressions to filter out unwanted users/profiles.
$UsersRegex = '' # For Example, you could use '(User\d\d|MyUser)$'
$Regex = "C:\\Users\\$UsersRegex"
$UserProfiles = Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\*" | Where ProfileImagePath -match "$Regex" | Select ProfileImagePath, PSChildName
$tmp = New-Object psobject -Property @{
	ProfileImagePath='C:\Users\Default'
	PSChildName='.DEFAULT'
}
$UserProfilesTmp = $UserProfiles
$UserProfiles = @($UserProfilesTmp,$tmp)

Foreach ($UserProfile in $UserProfiles){

    # Get paths for the layout file and an imminent backup file.
    $ProfilePath = $UserProfile.ProfileImagePath
    $shellpath = "$ProfilePath\AppData\Local\Microsoft\Windows\Shell\"
    $modFile = $shellPath + "LayoutModification.xml"
    $copyFile = $shellPath + "LayoutModification.xml.bak"

    # Make a backup of %LOCALAPPDATA%\Microsoft\Windows\Shell\LayoutModification.xml
    Copy-Item -Path $modFile -Destination $copyFile

    # Get the xml object of the layout file.
    [xml]$xmlDoc = Get-Content $modFile
    # Remove all items under the <TaskbarPinList> object.
    $xmlDoc.LayoutModificationTemplate.CustomTaskbarLayoutCollection.TaskbarLayout.TaskbarPinList.RemoveAll()
    # Save the changes to the original file (original settings are still retained in the backup.
    $xmlDoc.Save($modFile)

}
