# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack to edit numerous settings in File Explorer (AKA Windows Explorer).  These changes in settings include:
# * Make hidden folders and files visible.  Original source: https://www.isumsoft.com/windows-10/how-to-show-hidden-files-and-folders-in-windows-10.html
# * Make (common) file extensions visible.  Original source: https://superuser.com/questions/666891/script-to-set-hide-file-extensions
# * Disable OneDrive ads in File Explorer.  Original source: https://www.techjunkie.com/onedrive-ads-windows-10-file-explorer/
# * Disable recently opened items in the Quick Access menu.  This setting is apparently also affects the Start menu.
# * Disable frequently used folders in the Quick Access menu.
# * Set File Explorer to open to "This PC" instead of Quick Access.
# The purpose is to provide a tool to change these settings after a fresh install of Windows.
# Use Case: To be run once on each machine by an account with local admin privileges.  This script will automatically update all user profiles on the machine.  The change will take effect on the next logon for each user.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Regular Expressions to filter out unwanted users/profiles.
$UsersRegex = '' # For Example, you could use '(User\d\d|MyUser)$'
$Regex = "C:\\Users\\$UsersRegex"
$UserProfiles = @(Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\*" | Where ProfileImagePath -match "$Regex" | Select ProfileImagePath, PSChildName)
$tmp = New-Object psobject -Property @{
	ProfileImagePath='C:\Users\Default'
	PSChildName='.DEFAULT'
}
$UserProfiles += $tmp

# Temporary registry path to load profiles to (if necessary).
$TempHKUPath = 'HKU:\TEMP'
$TempHKUsersPath = 'HKEY_USERS\TEMP'

# Load HKEY_USERS Registry
New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS

Foreach ($UserProfile in $UserProfiles){
	# Query HKU to see if user registry is already loaded.  If so, update it right then and there.  Otherwise, load it manually to TEMP, update it in TEMP, then unload it manually.
	$ProfilePath = $UserProfile.ProfileImagePath
	$sid = $UserProfile.PSChildName
	$HKUPath = "HKU:\$sid"
	$HKUsersPath = "HKEY_USERS\$sid"
	
	$RegPath = ''
	$ManualLoad = $false
	Get-ItemProperty -Path $HKUPath
	if($?){
		# Registry is already loaded; we're good to go!
		$RegPath = $HKUsersPath
	}
	else{
		# Registry isn't loaded; we need to load it manually (and unload it later).
		$ManualLoad = $true
		$db = "$ProfilePath\NTUser.dat"
		reg load $TempHKUsersPath $db
		$RegPath = $TempHKUsersPath
	}
	
	# Control Panel -> Appearance and Personalization -> File Explorer Options -> View -> Show hidden files, folders, and drives (select this radio button)
	# Adding the registry key, setting it to a value of 1.  If the registry key does not exist already in the system, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced", 'Hidden', 1, [Microsoft.Win32.RegistryValueKind]::DWord)
	
	# Control Panel -> Appearance and Personalization -> File Explorer Options -> View -> Hide extensions for known file types (uncheck)
	# Adding the registry key, setting it to a value of 0.  If the registry key does not exist already in the system, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced", 'HideFileExt', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	
	# Control Panel -> Appearance and Personalization -> File Explorer Options -> View -> Show sync provider notifications (uncheck)
	# Adding the registry key, setting it to a value of 0.  If the registry key does not exist already in the system, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced", 'ShowSyncProviderNotifications', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	
	# Settings -> Personalization -> Start -> Show recently opened items in Jump Lists on Start or the taskbar and in File Explorer Quick Access (turn off)
	# Adding the registry key, setting it to a value of 0.  If the registry key does not exist already in the system, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced", 'Start_TrackDocs', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	
	# Control Panel -> Appearance and Personalization -> File Explorer Options -> General -> Show recently used files in Quick access (uncheck)
	# Adding the registry key, setting it to a value of 0.  If the registry key does not exist already in the system, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Explorer", 'ShowRecent', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	
	# Control Panel -> Appearance and Personalization -> File Explorer Options -> General -> Show frequently used folders in Quick access (uncheck)
	# Adding the registry key, setting it to a value of 0.  If the registry key does not exist already in the system, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Explorer", 'ShowFrequent', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	
	# Control Panel -> Appearance and Personalization -> File Explorer Options -> General -> Open File Explorer to: (select "This PC" from the drop-down list)
	# Adding the registry key, setting it to a value of 1.  If the registry key does not exist already in the system, then it will be created.  A value of 1 sets this option to "This PC"; a value of 2 sets it to Quick Access.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced", 'LaunchTo', 1, [Microsoft.Win32.RegistryValueKind]::DWord)
	
	# If we manually loaded the user's registry, let's go ahead and unload it.
	if($ManualLoad){
		reg unload $TempHKUsersPath
	}
}
