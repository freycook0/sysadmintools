# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack to add the Run As Administrator option for .ps1 files in Windows Explorer.  The idea is to provide a tool to add this configuration after a fresh install of Windows.
# Original source: https://www.thewindowsclub.com/add-run-administrator-ps1-file-context-menu
# Use Case: To be run once on each machine.  A reboot is required for the change to take effect.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-not $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Adding a string value to the registry key from the first step.  It's a string, signifying that this item in the context menu should have the UAC shield image.  If the registry key does not exist already in the system, then it will be created.
[Microsoft.Win32.Registry]::SetValue('HKEY_CLASSES_ROOT\Microsoft.PowerShellScript.1\Shell\Open\RunAs', 'HasLUAShield', '', [Microsoft.Win32.RegistryValueKind]::String)
# Adding a string value to the registry key from the first step.  It's a string, whose value is the code used to run a PS script as Admin.  If the registry key does not exist already in the system, then it will be created.
[Microsoft.Win32.Registry]::SetValue('HKEY_CLASSES_ROOT\Microsoft.PowerShellScript.1\Shell\Open\RunAs', 'command', @"
powershell.exe "-Command" "if((Get-ExecutionPolicy ) -ne 'AllSigned') { Set-ExecutionPolicy -Scope Process Bypass }; & '%1'"
"@, [Microsoft.Win32.RegistryValueKind]::String)
