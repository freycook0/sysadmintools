# If the script is called with the -AllUsers switch, then self-elevate and install for all users.
$machineInstall = $false
If($Args[0] -eq $true){
	$machineInstall = $true
}

# List of modules to install.
$modules = 'Map-PathToPhysicalDisk',
	'tmp'	# This is a temporary placeholder; there's a test in the ForEach loop that skips this module.
# TODO: Implement more modules then add them to the list.
# TODO: Remove the temporary placeholder.

# Get the install directory.
$installDirectory = ''
$modPath = 'PSModulePath'
If($machineInstall){
	$installDirectory = "$Env:ProgramFiles\WindowsPowerShell\Modules"
	$tmp1Directory = "$Env:ProgramFiles\WindowsPowerShell"
	# We'll assume that the "Program Files" directory exists.
	# $tmp2Directory = "$Env:ProgramFiles"
	If(-not (Test-Path $installDirectory)){
		If(-not (Test-Path $tmp1Directory)){
			New-Item $tmp1Directory
		}
		New-Item $installDirectory
	}
	# If the install directory is not in the machine's PS Module Path, then add it.
	$CurrentValue = [Environment]::GetEnvironmentVariable($modPath, 'Machine')
	# For some reason the \s in the parameter after the -match need to be escaped (with a \).
	If(-not ($CurrentValue -match $installDirectory.Replace('\','\\'))){
		[Environment]::SetEnvironmentVariable($modPath, "$CurrentValue;$installDirectory", 'Machine')
	}
}
Else{
	# Set the install directory to be within the user's "Documents" folder.  This will install the module for this user.
	$installDirectory = "$Env:USERPROFILE\Documents\WindowsPowerShell\Modules"
	$tmp1Directory = "$Env:USERPROFILE\Documents\WindowsPowerShell"
	# We'll assume that the user's home path exists and that it contains the "Documents" directory.
	# $tmp2Directory = "$Env:USERPROFILE\Documents"
	# $tmp3Directory = "$Env:USERPROFILE"
	If(-not (Test-Path $installDirectory)){
		If(-not (Test-Path $tmp1Directory)){
			New-Item $tmp1Directory
		}
		New-Item $installDirectory
	}
	# If the install directory is not in the user's PS Module Path, then add it.
	$CurrentValue = [Environment]::GetEnvironmentVariable($modPath)
	If(-not ($CurrentValue -match $installDirectory.Replace('\','\\'))){
		[Environment]::SetEnvironmentVariable($modPath, "$CurrentValue;$installDirectory")
	}
}

# For each module in the list, install the module in the appropriate directory.
ForEach($module in $modules){
	# Temporary check to skip the temporary module placeholder.
	If($module -eq 'tmp') {Continue}
	Copy-Item "$PSScriptRoot\$module" $installDirectory -Recurse -Force
}
