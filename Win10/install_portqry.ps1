# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script downloads the PortQry executable from Microsoft, extracts it, sets it up in a folder in "C:\Program Files (x86)", and adds said folder to the Path variable.  This makes PortQry available to users on the machine.  PortQry is a handy network utility, which can be used to indicate whether a specific port on a host/IP address is accessible.  The idea is to provide a tool to automate this setup after a fresh install of Windows.
# Use Case: To be run by an administrator, once on each machine.  A reboot is required for the change to take effect.
# Requirements: 7-Zip must be installed on the target machine, in order to extract the final executable.  There's no other way to do it other than 7-Zip or .exe self-extraction.  We don't want to use self-extraction, as the GUI can't be automated, and command-line options don't work (as per my testing; perhaps follow-up is warranted).

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# 
# Should we bother to identify the system drive?  Or is assuming that the C: drive is the system drive sufficient?

# Check if 7-Zip is installed, and if so, find the installation directory.  Install check found at: https://stackoverflow.com/questions/27348077/how-to-check-if-7zip-is-installed-with-powershell-my-method-fails
$7ZipDirectory = ''
$installs = Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | ?{$_.DisplayName -like "7-Zip*"}
if($installs -ne $null){
	# We'll assume that a shortcut was added to the Start Menu during the 7-Zip install.  We'll follow that shortcut, to find the 7-Zip install directory.
	$sh = New-Object -COM WScript.Shell
	# $targetPath = $sh.CreateShortcut('C:\ProgramData\Microsoft\Windows\Start Menu\Programs\7-Zip\7-Zip File Manager.lnk').TargetPath
	$targetPath = $sh.CreateShortcut("$env:ProgramData\Microsoft\Windows\Start Menu\Programs\7-Zip\7-Zip File Manager.lnk").TargetPath
	$7zipDirectory = (Get-Item $targetPath).DirectoryName # Should we use Directory or DirectoryName?
}
if($7zipDirectory -eq ''){
	# 7-Zip is not installed, or we are otherwise unable to locate the 7-Zip install directory.  We are unable to unpack the PortQry executable for installation.
	# TODO: Add in big error message, and wait until the user presses a key to continue or something.
	exit
}

# Create install directory.  Due to inherited file permissions, I think "C:\Program Files (x86)\" is the way to go.
$installFolder = ''
# $installFolder = (New-Item -Path 'C:\Program Files (x86)' -Name PortQry -ItemType "directory").FullName
$installFolder = (New-Item -Path ${env:ProgramFiles(x86)} -Name PortQry -ItemType "directory").FullName
if($installFolder -eq ''){
	# Failed to create install directory.
	# TODO: Add in big error message, and wait until the user presses a key to continue or something.
	exit
}

# Grab the user's home directory, and set up a tmp directory to download to/extract from.
# $workingFolder = (Get-Item ((pwd).Path)).FullName
# $workingFolder = $installFolder
$workingFolder = [System.IO.Path]::GetTempPath()
cd $workingFolder
New-Item -Path $workingFolder -Name "tmp" -ItemType "directory"
$deleteTmp = $?
# $tmpFolder = "$workingFolder\tmp"
$tmpFolder = $workingFolder + 'tmp'

$workingFolder
$tmpFolder

# Set the TLS protocol version to 1.2.  It may not be necessary, but it's handy to do so.
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12

# This is the URL to download PortQry from Microsoft.  The URL will need to be updated if Microsoft ever changes it.
$portqryUrl = 'https://download.microsoft.com/download/0/d/9/0d9d81cf-4ef2-4aa5-8cea-95a935ee09c9/PortQryV2.exe'
$portqryFile = "$tmpFolder\PortQryV2.exe"
$webClient = New-Object System.Net.WebClient
$webClient.DownloadFile($portqryUrl,$portqryFile)

# Use 7-Zip to extract PortQry.exe from the nested self-extracting exe's.  Note: 7-Zip kind of craps the bed in a lot of cases throughout my testing; I was able to get this working by placing the self-extracting exe's in a temporary directory for each extraction.
& "$7ZipDirectory\7z.exe" e "$tmpFolder\PortQryV2.exe"
Remove-Item "$tmpFolder\PortQryV2.exe" -Force
Move-Item "$workingFolder\PORTQR~1.EXE" "$tmpFolder\PORTQR~1.EXE"
& "$7ZipDirectory\7z.exe" e "$tmpFolder\PORTQR~1.EXE" PortQry.exe
Remove-Item "$tmpFolder\PORTQR~1.EXE" -Force
if($deleteTmp){
	Remove-Item "$tmpFolder" -Recurse -Force
}

# Move the executable to the install folder.
Move-Item "$workingFolder\PortQry.exe" "$installFolder\PortQry.exe"

# Add install directory to default path.
$oldPath = (Get-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).Path
# $newPath = "$oldPath;C:\Program Files (x86)\PortQry"
$newPath = "$oldPath;${env:ProgramFiles(x86)}\PortQry"
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value $newPath
