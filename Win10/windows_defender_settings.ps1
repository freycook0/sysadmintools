# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack to edit numberous settings in Windows Defender.  These changes include:
# * Disable notifications advising the user to link their computer account with a Microsoft account.  Original source: https://old.reddit.com/r/sysadmin/comments/8h051d/win10_1803_remove_forced_edge_shortcut_from/dygkex2/
# * Disable SmartScreen within the contexts of the local files and apps, within Microsoft Edge, and within any app from the Microsoft Store.  WARNING: SmartScreen is a tool designed as part of the security systems integrated into Windows; decide carefully before running this script.  Original source: https://winaero.com/blog/disable-smartscreen-windows-10-creators-update/
# The purpose is to provide a tool to change these settings after a fresh install of Windows.
# Use Case: To be run once on each machine by an account with local admin privileges.  This script will automatically update all user profiles on the machine.  The change will take effect on the next logon for each user.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Begin with the machine-based registry changes, then proceed with enumerating and editing all user profiles.

# This item does not work on Windows 10 1909 (or later, probably).
# Adding the registry key, setting it to a value of 0.  If the registry key does not exist already in the system, then it will be created.
[Microsoft.Win32.Registry]::SetValue('HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Security Health\State', 'AccountProtection_MicrosoftAccount_Disconnected', 0, [Microsoft.Win32.RegistryValueKind]::DWord)

# Windows Defender -> App & browser control -> Check apps and files (Set to "Off")
[Microsoft.Win32.Registry]::SetValue('HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer', 'SmartScreenEnabled', 'Off', [Microsoft.Win32.RegistryValueKind]::String)

# Regular Expressions to select certain users/profiles (or to filter out unwanted users/profiles).
$UsersRegex = '' # For Example, you could use '(User\d\d|MyUser)$'
$Regex = "C:\\Users\\$UsersRegex"
$UserProfiles = @(Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\*" | Where ProfileImagePath -match "$Regex" | Select ProfileImagePath, PSChildName)
$tmp = New-Object psobject -Property @{
	ProfileImagePath='C:\Users\Default'
	PSChildName='.DEFAULT'
}
$UserProfiles += $tmp

# Temporary registry path to load profiles to (if necessary).
$TempHKUPath = 'HKU:\TEMP'
$TempHKUsersPath = 'HKEY_USERS\TEMP'

# Load HKEY_USERS Registry
New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS

Foreach ($UserProfile in $UserProfiles){
	# Query HKU to see if user registry is already loaded.  If so, update it right then and there.  Otherwise, load it manually to TEMP, update it in TEMP, then unload it manually.
	$ProfilePath = $UserProfile.ProfileImagePath
	$sid = $UserProfile.PSChildName
	$HKUPath = "HKU:\$sid"
	$HKUsersPath = "HKEY_USERS\$sid"
	
	$RegPath = ''
	$ManualLoad = $false
	Get-ItemProperty -Path $HKUPath
	if($?){
		# Registry is already loaded; we're good to go!
		$RegPath = $HKUsersPath
	}
	else{
		# Registry isn't loaded; we need to load it manually (and unload it later).
		$ManualLoad = $true
		$db = "$ProfilePath\NTUser.dat"
		reg load $TempHKUsersPath $db
		$RegPath = $TempHKUsersPath
	}
	
	# Windows Defender -> App & browser control -> SmartScreen for Microsoft Edge (Set to "Off")
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\PhishingFilter", 'EnabledV9', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	
	# Windows Defender -> App & browser control -> SmartScreen for Microsoft Store apps (Set to "Off")
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\AppHost", 'EnableWebContentEvaluation', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
		
	# If we manually loaded the user's registry, let's go ahead and unload it.
	if($ManualLoad){
		reg unload $TempHKUsersPath
	}
}
