# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script disables the "fast boot" in Windows 10.  This prevents both bootup performance issues and issues arising from an incomplete shutdown. The idea is to provide a tool to change this setting after a fresh install of Windows.
# Use Case: To be run once on each machine by an account with local admin privileges.  A reboot is required for the change to take effect.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

powercfg.exe /H off
