# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack to edit numberous keybaord-related settings.  These changes include:
# * Disable the Sticky Keys shortcut (press Shift 5 times).  Original source: https://github.com/farag2/Windows-10-Setup-Script
# * Disable the Toggle Keys shortcut (hold Num Lock for 5 secodns).
# * Disable the Filter Keys shortcut (hold right Shift key for 8 seconds).
# * Disable the Help function when the F1 key is pressed.  Original source: https://github.com/farag2/Windows-10-Setup-Script
# The purpose is to provide a tool to change these settings after a fresh install of Windows.
# Use Case: To be run once on each machine by an account with local admin privileges.  This script will automatically update all user profiles on the machine.  The change will take effect after a reboot.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Regular Expressions to select certain users/profiles (or to filter out unwanted users/profiles).
$UsersRegex = '' # For Example, you could use '(User\d\d|MyUser)$'
$Regex = "C:\\Users\\$UsersRegex"
$UserProfiles = @(Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\*" | Where ProfileImagePath -match "$Regex" | Select ProfileImagePath, PSChildName)
$tmp = New-Object psobject -Property @{
	ProfileImagePath='C:\Users\Default'
	PSChildName='.DEFAULT'
}
$UserProfiles += $tmp

# Temporary registry path to load profiles to (if necessary).
$TempHKUPath = 'HKU:\TEMP'
$TempHKUsersPath = 'HKEY_USERS\TEMP'

# Load HKEY_USERS Registry
New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS

Foreach ($UserProfile in $UserProfiles){
	# Query HKU to see if user registry is already loaded.  If so, update it right then and there.  Otherwise, load it manually to TEMP, update it in TEMP, then unload it manually.
	$ProfilePath = $UserProfile.ProfileImagePath
	$sid = $UserProfile.PSChildName
	$HKUPath = "HKU:\$sid"
	$HKUsersPath = "HKEY_USERS\$sid"
	
	$RegPath = ''
	$ManualLoad = $false
	Get-ItemProperty -Path $HKUPath
	if($?){
		# Registry is already loaded; we're good to go!
		$RegPath = $HKUsersPath
	}
	else{
		# Registry isn't loaded; we need to load it manually (and unload it later).
		$ManualLoad = $true
		$db = "$ProfilePath\NTUser.dat"
		reg load $TempHKUsersPath $db
		$RegPath = $TempHKUsersPath
	}
	
	# Settings -> Ease of Access -> Keyboard
	# "Allow the shortcut key to start Sticky Keys" / "Press the Shift key five times to turn Sticky Keys on or off" (Uncheck the box)
	# 506 = off; 510 = on
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Accessibility\StickyKeys", 'Flags', '506', [Microsoft.Win32.RegistryValueKind]::String)
	
	# Settings -> Ease of Access -> Keyboard
	# "Allow the shorcut key to start Toggle Keys" / "Press and hold the Num Lock key for five seconds to turn on Toggle Keys" (Uncheck the box)
	# 58 = off; 62 = on
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Accessibility\ToggleKeys", 'Flags', '58', [Microsoft.Win32.RegistryValueKind]::String)
	
	# Settings -> Ease of Access -> Keyboard
	# "Allow the shorcut key to start Filter Keys" / "Press and hold the right Shift key for eight seconds to turn on Filter Keys" (Uncheck the box)
	# 122 = off; 126 = on
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Control Panel\Accessibility\Keyboard Response", 'Flags', '122', [Microsoft.Win32.RegistryValueKind]::String)
	
	# Disable the Help function when the F1 key is pressed.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Classes\Typelib\{8cec5860-07a1-11d9-b15e-000d56bfe6ee}\1.0\0\win64", '(default)', '', [Microsoft.Win32.RegistryValueKind]::String)
	
	# If we manually loaded the user's registry, let's go ahead and unload it.
	if($ManualLoad){
		reg unload $TempHKUsersPath
	}
}

# 
