# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack to change Taskbar settings.  The idea is to provide a tool to change this setting after a fresh install of Windows.
# Please note that the Taskbar settings below were decided for the author's personal preference.
# Original Sources:	https://www.tenforums.com/tutorials/2854-hide-show-search-box-search-icon-taskbar-windows-10-a.html#option3
# https://www.tenforums.com/tutorials/2853-hide-show-task-view-button-taskbar-windows-10-a.html#option3
# https://www.reddit.com/r/sysadmin/comments/nubi7f/kb5003214_adds_taskbar_junk_and_broke_dual_display/h0xsm4z/
# Use Case: To be run once on each machine by an account with local admin privileges.  This script will automatically update all user profiles on the machine.  The change will take effect on the next logon for each user.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-not $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Regular Expressions to filter out unwanted users/profiles.
$UsersRegex = '' # For Example, you could use '(User\d\d|MyUser)$'
$Regex = "C:\\Users\\$UsersRegex"
$UserProfiles = @(Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\*" | Where ProfileImagePath -match "$Regex" | Select ProfileImagePath, PSChildName)
$tmp = New-Object psobject -Property @{
	ProfileImagePath='C:\Users\Default'
	PSChildName='.DEFAULT'
}
$UserProfiles += $tmp

# Temporary registry path to load profiles to (if necessary).
$TempHKUPath = 'HKU:\TEMP'
$TempHKUsersPath = 'HKEY_USERS\TEMP'

# Load HKEY_USERS Registry
New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS

Foreach ($UserProfile in $UserProfiles){
	# Query HKU to see if user registry is already loaded.  If so, update it right then and there.  Otherwise, load it manually to TEMP, update it in TEMP, then unload it manually.
	$ProfilePath = $UserProfile.ProfileImagePath
	$sid = $UserProfile.PSChildName
	$HKUPath = "HKU:\$sid"
	$HKUsersPath = "HKEY_USERS\$sid"
	
	$RegPath = ''
	$ManualLoad = $false
	Get-ItemProperty -Path $HKUPath
	if($?){
		# Registry is already loaded; we're good to go!
		# $RegPath = $HKUPath
		$RegPath = $HKUsersPath
	}
	else{
		# Registry isn't loaded; we need to load it manually (and unload it later).
		$ManualLoad = $true
		$db = "$ProfilePath\NTUser.dat"
		reg load $TempHKUsersPath $db
		# $RegPath = $TempHKUPath
		$RegPath = $TempHKUsersPath
	}
	
	# Right-click on Taskbar -> Search -> Hidden
	# Hiding the Search box on the Taskbard.  Adding to the registry key, setting it to 0.  If the registry key does not exist already in the system, then it will be created.
	# HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Search DWORD SearchboxTaskbarMode 0
	[Microsoft.Win32.Registry]::SetValue("$RegPath\SOFTWARE\Microsoft\Windows\CurrentVersion\Search", 'SearchboxTaskbarMode', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	# 0 = Hidden
	# 1 = Show search icon
	# 2 = Show search box

	# Right-click on Taskbar -> Show Task View button (uncheck)
	# Hiding the Task View button.  Adding to the registry keys, setting it to 0.  If the registry key does not exist already in the system, then it will be created.
	# HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced DWORD ShowTaskViewButton 0
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced", 'ShowTaskViewButton', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	# 0 = Hide "Task view" button
	# 1 = Show "Task view" button
	
	# Right-click on Taskbar -> Show Cortana button (uncheck)
	# Hiding the Cortana button.  Adding to the registry keys, setting it to 0.  If the registry key does not exist already in the system, then it will be created.
	# HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced DWORD ShowCortanaButton 0
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced", 'ShowCortanaButton', 0, [Microsoft.Win32.RegistryValueKind]::DWord)
	# 0 = Hide "Cortana" button
	# 1 = Show "Cortana" button
	
	# Settings -> Personalization -> Taskbar -> Turn system icons on or off -> Meet Now (turn off)
	# Hiding the Meet Now icon.  Adding to the registry keys, setting it to 0.  If the registry key does not exist already in the system, then it will be created.
	# HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer DWORD HideSCAMeetNow 1
	[Microsoft.Win32.Registry]::SetValue("$RegPath\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer", 'HideSCAMeetNow', 1, [Microsoft.Win32.RegistryValueKind]::DWord)
	# 0 = Show "Meet Now" icon
	# 1 = Hide "Meet Now" icon
	
	# Right-click on Taskbar -> News and interests -> Turn Off
	# Turning off "News and interests" on the taskbar.  Adding to the registry key, setting it to a value of 2.  If the registry key does not exist already in the system, then it will be created.
	[Microsoft.Win32.Registry]::SetValue("$RegPath\SOFTWARE\Microsoft\Windows\CurrentVersion\Feeds", 'ShellFeedsTaskbarViewMode', 2, [Microsoft.Win32.RegistryValueKind]::DWord)
	# HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Feeds DWORD ShellFeedsTaskbarViewMode Value 2

	# Force Explorer.exe to restart, causing the taskbar to update after the registry changes.
	Stop-Process -Name explorer -Force
	
	# If we manually loaded the user's registry, let's go ahead and unload it.
	if($ManualLoad){
		reg unload $TempHKUsersPath
	}
}
