# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script installs and uninstalls various optional features in Windows to make certain desired features available and to remove various legacy or undesired features.  The idea is to provide a tool to change these feature installs after a fresh install of Windows.
# Original Source: https://windowsloop.com/uninstall-internet-explorer-in-windows-10/
# Note: To view which optional features are installed, I recommend the following:
# Get-WindowsOptionalFeature -Online | Where-Object State -eq Enabled | Sort-Object FeatureName | Select FeatureName
# Or, for a full list of features and whether or not they are installed:
# Get-WindowsOptionalFeature -Online | Sort-Object FeatureName | Select FeatureName, State
# Use Case: To be run once on each machine by an account with local admin privileges.  A reboot is required for the changes to take effect.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-not $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# List of named features to be installed.
$installFeatures =	'SimpleTCP',	# "Simple TCPIP services (i.e. echo, daytime etc)" enables Wake-on-LAN capabilities.
					'Containers-DisposableClientVM'	# "Windows Sandbox" enables running applications in a temporary sandbox without setting up a full vm.
ForEach($feature in $installFeatures){
	Write-Host "Feature: $feature"
	# If the feature is currently installed, then remove it.  Otherwise; skip it.
	If((Get-WindowsOptionalFeature -FeatureName $feature -Online).State -ne 'Enabled'){
		Write-Host "Installing $feature..."
		Enable-WindowsOptionalFeature -FeatureName $feature -Online -NoRestart
	}
	Else{
		Write-Host "Feature $feature already installed."
	}
}

# List of named features to be uninstalled.
$uninstallFeatures =	'DirectPlay', # "DirectPlay": 
						'Internet-Explorer-Optional-amd64',	# "Internet Explorer 11" isn't used for much anymore, other than maybe legacy (super old) web apps.
						'LegacyComponents', # "Legacy Components": 
						'MediaPlayback',	# "Media Features": I use 3rd party apps for media playback.
						'Printing-XPSServices-Features',	# "Microsoft XPS Document Writer": I do use the Print-to-PDF feature, but never to XPS.
						'WindowsMediaPlayer'	# "Windows Media Player": I use 3rd party apps for media playback.
ForEach($feature in $uninstallFeatures){
	Write-Host "Feature: $feature"
	# If the feature is currently installed, then remove it.  Otherwise; skip it.
	If((Get-WindowsOptionalFeature -FeatureName $feature -Online).State -ne 'Disabled'){
		Write-Host "Uninstalling $feature..."
		Disable-WindowsOptionalFeature -FeatureName $feature -Online -Remove -NoRestart
	}
	Else{
		Write-Host "Feature $feature not installed."
	}
}
