# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script kills and then uninstalls OneDrive from the machine.  The justification for this is that not all users of Windows 10 outside of an enterprise utilize OneDrive for cloud storage.  The idea is to provide a tool to change this setting after a fresh install of Windows.
# Original source: https://lifehacker.com/how-to-completely-uninstall-onedrive-in-windows-10-1725363532
# Use Case: To be run once on each machine.  A reboot is required for the change to take effect.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Kill any running instances of OneDrive
taskkill /f /im OneDrive.exe

# Find out where OneDrive is installed (determined by whether Windows is 32-bit or 64-bit.
if(Test-Path "$env:SystemRoot\System32\OneDriveSetup.exe") {
	Start-Process -FilePath "$env:SystemRoot\System32\OneDriveSetup.exe" -ArgumentList "/uninstall" -Wait -Verb RunAs
}
if(Test-Path "$env:SystemRoot\SysWOW64\OneDriveSetup.exe") {
	Start-Process -FilePath "$env:SystemRoot\SysWOW64\OneDriveSetup.exe" -ArgumentList "/uninstall" -Wait -Verb RunAs
}
