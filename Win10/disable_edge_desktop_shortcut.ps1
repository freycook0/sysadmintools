# ---------------------------------------------------
# Header
# ---------------------------------------------------

# This script serves as a registry hack to disable the creation of a shortcut to Edge on the desktop on login.  The idea is to provide a tool to change this setting after a fresh install of Windows.
# Original source: https://old.reddit.com/r/sysadmin/comments/8h051d/win10_1803_remove_forced_edge_shortcut_from/dygkex2/
# Use Case: To be run once on each machine.  A reboot is required for the change to take effect.

# ---------------------------------------------------
# Self-Elevation
# ---------------------------------------------------

$Principal = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent())
If(-NOT $Principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
	# Relaunch as an elevated process:
	Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
	exit
}

# ---------------------------------------------------
# Script
# ---------------------------------------------------

# Adding the registry key, setting it to a value of 1.  This action is done with the -Force parameter, so if the key already exists, then it is overwritten.  If the registry key does not exist already in the system, then it will be created.
[Microsoft.Win32.Registry]::SetValue('HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer', 'DisableEdgeDesktopShortcutCreation', 1, [Microsoft.Win32.RegistryValueKind]::DWord)
